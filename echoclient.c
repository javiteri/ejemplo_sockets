#include "csapp.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>


int main(int argc, char **argv)
{
	int clientfd;
	char *port;
	char *host, buf[MAXLINE];
	char *archivo;
	rio_t rio;
	size_t n;

	if (argc != 4) {
		fprintf(stderr, "usage: %s <host> <port> <archivo>\n", argv[0]);
		exit(0);
	}
	host = argv[1];
	port = argv[2];
	//Nombre del archivo
	archivo = argv[3];
	clientfd = Open_clientfd(host, port);
	Rio_readinitb(&rio, clientfd);
	strcpy(buf, archivo);

		
	printf("buf es %s\n", buf );
	Rio_writen(clientfd,buf,sizeof(buf));
	bzero((char *)&buf,sizeof(buf));
	Rio_readlineb(&rio,buf,sizeof(buf));
	Fputs(buf, stdout);
	
	if(strcmp(buf, "0\n") == 0){
		printf("No encontro el archivo\n");
	}else if(strcmp(buf, "1\n") == 0){
		printf("El archivo existe\n");
		FILE *archivoCreado;
		archivoCreado = fopen("Creado.txt", "w+");
		while(1){
			Rio_readlineb(&rio,buf,sizeof(buf));
			if ( strcmp(buf,"fin\n") == 0 ){
				break;
			}
			fputs(buf, archivoCreado);

		}
		fclose(archivoCreado);

	}



	Close(clientfd);
	exit(0);
}
